import UIKit

protocol Builder {
    static func createMainModule() -> UIViewController
    static func createDetailModule(hero: Heroes?, comics: [ComicName]?) -> UIViewController
    static func createFullScreenModule(image: UIImage) -> UIViewController
}

class ModuleBuilder: Builder {
    static func createMainModule() -> UIViewController {
        let view = HeroesViewController()
        let networkService = NetworkHeroesService()
        let presenter = HeroesPresenter(view: view, networkService: networkService)
        view.presenter = presenter
        return view
    }
    
    static func createDetailModule(hero: Heroes?, comics: [ComicName]?) -> UIViewController {
        let view = DetaileHeroesViewController()
        let networkService = NetworkHeroesService()
        let presenter = DetailPresenter(view: view, networkService: networkService, hero: hero, comics: comics)
        view.presenter = presenter
        return view
    }
    
    static func createFullScreenModule(image: UIImage) -> UIViewController {
        let view = FullScreenImageHero(image: image)
        return view
    }
}
