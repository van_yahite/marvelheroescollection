import UIKit

extension UIImageView {
    func loadImage(from url: String) {
        HeroesImageService.shared.imageLoad(urlImage: url) { [weak self] result in
            switch result {
            case .success(let image):
                DispatchQueue.main.async {
                    self?.image = image
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
}
