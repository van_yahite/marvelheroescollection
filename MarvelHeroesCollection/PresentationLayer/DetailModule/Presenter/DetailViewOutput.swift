import Foundation

protocol DetailViewOutput: AnyObject {
    func setData()
}
