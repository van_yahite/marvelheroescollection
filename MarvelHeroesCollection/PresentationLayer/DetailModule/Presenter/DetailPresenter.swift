//
//  DetailPresenter.swift
//  MarvelHeroesCollection
//
//  Created by Иван Сатинов on 22.09.22.
//

import Foundation

class DetailPresenter: DetailViewOutput {
    
    private let networkService: NetworkHeroesServiceProtocol
    private weak var view: DetailViewInput?
    private var hero: Heroes?
    private var comics: [ComicName]?
    
    init (view: DetailViewInput, networkService: NetworkHeroesServiceProtocol, hero: Heroes?, comics: [ComicName]?) {
        self.view = view
        self.networkService = networkService
        self.hero = hero
        self.comics = comics
    }
    
    func setData() {
        guard let hero = hero, let comics = comics else { return }
        self.view?.setupViewHeroes(hero: hero, comics: comics)
    }
}
