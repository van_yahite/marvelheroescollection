import Foundation

protocol DetailViewInput: AnyObject {
    func setupViewHeroes(hero: Heroes, comics: [ComicName])
}

