import UIKit

final class DetaileHeroesViewController: UIViewController {
    
    @IBOutlet private weak var imageHero: UIImageView!
    @IBOutlet private weak var heroesName: UILabel!
    @IBOutlet private weak var descriptionHeroes: UILabel!
    @IBOutlet private weak var stackHeroesComics: UIStackView!
    
    private var hero: Heroes?
    private var comics: [ComicName]?
    var presenter: DetailViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.setData()
        guard let hero = hero, let comics = comics else { return }
        setupViewHeroes(hero: hero, comics: comics)
    }
    
    @IBAction func tapGestureFullHeroImage(_ sender: Any) {
        guard let imageHero = imageHero.image else { return }
        let fullScreanView = ModuleBuilder.createFullScreenModule(image: imageHero)
        present(fullScreanView, animated: true, completion: nil)
    }
    
}

// MARK: - DetailPresenterInput
extension DetaileHeroesViewController: DetailViewInput {
    
    func setupViewHeroes(hero: Heroes, comics: [ComicName]) {
        imageHero.isUserInteractionEnabled = true
        heroesName.text = hero.name
        descriptionHeroes.text = hero.description
        let imageUrl = hero.thumbnail.imageURL + "." + hero.thumbnail.imageType
        imageHero.loadImage(from: imageUrl)
        let testLable = UILabel()
        testLable.text = "text_Comics".localized()
        testLable.font = UIFont.systemFont(ofSize: 30)
        testLable.font = UIFont.boldSystemFont(ofSize: 30)
        stackHeroesComics.addArrangedSubview(testLable)
        if hero.description.isEmpty {
            descriptionHeroes.text = "text_NoData".localized()
        }
        if comics.isEmpty {
            let comicsLableisEmpty = UILabel()
            comicsLableisEmpty.text = "text_NoData".localized()
            stackHeroesComics.addArrangedSubview(comicsLableisEmpty)
        }
        for comic in comics {
            let comicsLabel = UILabel()
            comicsLabel.numberOfLines = 0
            comicsLabel.textColor = .black
            comicsLabel.backgroundColor = .gray
            comicsLabel.font = UIFont.systemFont(ofSize: 20)
            comicsLabel.text = comic.name
            stackHeroesComics.addArrangedSubview(comicsLabel)
        }
    }
}
