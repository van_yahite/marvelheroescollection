import UIKit
import SwiftUI

final class FullScreenImageHero: UIViewController {
    
    @IBOutlet private weak var fullImage: UIImageView!
    @IBOutlet private weak var scrollImage: UIScrollView!
    
    private var image: UIImage
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupImage()
        scrollImage.delegate = self
    }

    init(image: UIImage) {
        let bundle = Bundle(for: type(of: self))
        self.image = image
        super.init(nibName: "FullScreenImageHero", bundle: bundle)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func buttonBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - UIScrollViewDelegate
extension FullScreenImageHero: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        fullImage
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        if scrollImage.zoomScale > 1 {
            if let image = fullImage.image {
                let ratioW = fullImage.frame.width / image.size.width
                let ratioH = fullImage.frame.height / image.size.height
                let ratio = ratioW < ratioH ? ratioW : ratioH
                let newWidth = image.size.width * ratio
                let newHeight = image.size.height * ratio
                let conditionLeft = newWidth * scrollImage.zoomScale > fullImage.frame.width
                let left = 0.5 * (conditionLeft ? newWidth - fullImage.frame.width : (scrollImage.frame.width - scrollImage.contentSize.width))
                let conditioTop = newHeight * scrollImage.zoomScale > fullImage.frame.height
                let top = 0.5 * (conditioTop ? newHeight - fullImage.frame.height : (scrollImage.frame.height - scrollImage.contentSize.height))
                scrollImage.contentInset = UIEdgeInsets(top: top, left: left, bottom: top, right: left)
            }
        } else {
            scrollImage.contentInset = .zero
        }
    }
}

// MARK: - Private Methods
extension FullScreenImageHero {
    private func setupImage() {
        fullImage.image = image
    }
}
