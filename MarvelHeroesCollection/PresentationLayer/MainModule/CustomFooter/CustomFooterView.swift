//
//  CustomFooterView.swift
//  MarvelHeroesCollection
//
//  Created by Иван Сатинов on 21.09.22.
//

import UIKit

class CustomFooterView: UICollectionReusableView {

    @IBOutlet weak var spiner: UIActivityIndicatorView!
    
    func startSpinner() {
            spiner?.startAnimating()
        }
    
        func stopSpinner() {
            spiner?.stopAnimating()
        }
}
