//
//  HeroesPresenter.swift
//  MarvelHeroesCollection
//
//  Created by Иван Сатинов on 16.09.22.
//

import Foundation

final class HeroesPresenter: MainViewOutput {
    
    weak private var view: MainViewInput?
    private var networkHeroesService: NetworkHeroesServiceProtocol
    var allHeroes = [Heroes]()
    
    init(view: MainViewInput, networkService: NetworkHeroesServiceProtocol) {
        self.view = view
        self.networkHeroesService = networkService
    }
    
    func viewDidLoad() {
        getData()
    }
    
    func getData() {
        networkHeroesService.request(offset: allHeroes.count) { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case .success(let searchResponse):
                    self.allHeroes.append(contentsOf: searchResponse.data.results)
                    self.view?.succes(hero: self.allHeroes)
                case .failure(let error):
                    self.view?.failure(error: error)
                }
            }
        }
    }
}
