import Foundation

protocol MainViewOutput: AnyObject {
    var allHeroes: [Heroes] { get set }
    func viewDidLoad()
    func getData()
}
