import Foundation

protocol MainViewInput: AnyObject {
    func succes(hero: [Heroes])
    func failure(error: Error)
}
