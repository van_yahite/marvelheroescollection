import UIKit

final class HeroesViewController: UIViewController {
    private enum Constants {
        static let cellIdentifier = "HeroesCollectionViewCell"
        static let footerIdentifier = "Footer"
        static let footerFileName = "CustomFooterView"
        static let edgeInset: CGFloat = 8
        static let heightMultiplication = 1.5
        static let widthMultiplication = 0.5
        static let widthDefault: Double = 16
    }
    
    @IBOutlet var heroesCollectionView: UICollectionView!
    
    private let searchController = UISearchController(searchResultsController: nil)
    private var filteredHeroes = [Heroes]()
    private var allHeroes = [Heroes]()
    private var spinner = CustomFooterView()
    var presenter: MainViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupHeroesCollection()
        presenter?.viewDidLoad()
        setupSearchBar()
    }
}

// MARK: - UICollectionViewDataSource
extension HeroesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        filteredHeroes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let hero = filteredHeroes[indexPath.row]
        let urlImage = hero.thumbnail.imageURL + "." +  hero.thumbnail.imageType
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.cellIdentifier, for: indexPath) as? HeroesCollectionViewCell else { return UICollectionViewCell() }
        cell.configure(name: hero.name, imageURL: urlImage)
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension HeroesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            guard let footer = heroesCollectionView.dequeueReusableSupplementaryView(ofKind: kind , withReuseIdentifier: Constants.footerIdentifier, for: indexPath) as? CustomFooterView else { return UICollectionReusableView() }
            spinner = footer
            
            return footer
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showHeroDetails(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if filteredHeroes.count - 1 == indexPath.row {
            presenter?.viewDidLoad()
            spinner.startSpinner()
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension HeroesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width * Constants.widthMultiplication) - Constants.widthDefault
        let height = width * Constants.heightMultiplication
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: Constants.edgeInset, left: Constants.edgeInset, bottom: Constants.edgeInset, right: Constants.edgeInset)
    }
}

// MARK: - UISearchResultsUpdating
extension HeroesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            filterContentForSearchText(text)
        }
    }
}

// MARK: - MainViewInput
extension HeroesViewController: MainViewInput {
    func succes(hero: [Heroes]) {
        filteredHeroes = hero
        heroesCollectionView.reloadData()
    }
    
    func failure(error: Error) {
        debugPrint(error.localizedDescription)
    }
}

// MARK: - Private Methods
private extension HeroesViewController {
    
    func setupHeroesCollection() {
        let nib = UINib(nibName: Constants.cellIdentifier, bundle: nil)
        heroesCollectionView.delegate = self
        heroesCollectionView.dataSource = self
        heroesCollectionView.register(UINib(nibName: Constants.footerFileName, bundle: nil), forCellWithReuseIdentifier: Constants.footerFileName)
        heroesCollectionView.register(nib, forCellWithReuseIdentifier: Constants.cellIdentifier)
    }
    
    func showHeroDetails(indexPath: IndexPath) {
        let hero = presenter?.allHeroes[indexPath.row]
        let comics = presenter?.allHeroes[indexPath.row].comics.items
        let detailViewComtroller = ModuleBuilder.createDetailModule(hero: hero, comics: comics)
        navigationController?.pushViewController(detailViewComtroller, animated: true)
    }
    
    func setupSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "text_Search".localized()
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func filterContentForSearchText(_ searchText: String) {
        guard !searchText.isEmpty else {
            filteredHeroes = allHeroes
            heroesCollectionView.reloadData()
            return
        }
        filteredHeroes = allHeroes.filter({ $0.name.lowercased().contains(searchText.lowercased())})
        heroesCollectionView.reloadData()
    }
}
